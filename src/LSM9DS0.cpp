/**
 * @file LSM9DS0.cpp
 * @author Sam Povilus(povilus@povil.us)
 * @brief 
 * @version 0.1
 * @date 2020-06-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

/**
 * I'm getting a defenition of struct sched_param from both 
 * 		/usr/arm-linux-gnueabihf/include/bits/sched.h:74 (included by C++ headers string and memory)
 * 		/usr/arm-linux-gnueabihf/include/linux/sched/types.h:7 (included by me to get struct sched_attr)
 * 		Defining _BITS_TYPES_STRUCT_SCHED_PARAM will allow me to elemenate one 
 */
#define _BITS_TYPES_STRUCT_SCHED_PARAM 1

#include <string>
#include <memory>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <linux/sched.h>
#include <linux/sched/types.h>
#include <syscall.h>
#include <stdbool.h>
#include <thread>
#include <system_error>
#include <iostream>

#include "gyro.hpp"
#include "accelerometer.hpp"
#include "motor.hpp"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static const char *accDevice = "/opt/robot/sensors/accelerometer_spi";
static const char *gyroDevice = "/opt/robot/sensors/gyro_spi";
static uint32_t mode;
static uint8_t bits = 8;
static uint32_t speed = 10000000L;
static int verbose;
static int loopCount = 10;
static long long lDelay = 100000;
static bool sleepInsteadOfSchedDeadline = false;
static float frequency = 10000LL;
static std::string outputFileName;
static FILE *fptr;

int sched_setattr(pid_t pid,
				  const struct sched_attr *attr,
				  unsigned int flags)
{
	return syscall(__NR_sched_setattr, pid, attr, flags);
}

int sched_getattr(pid_t pid,
				  struct sched_attr *attr,
				  unsigned int size,
				  unsigned int flags)
{
	return syscall(__NR_sched_getattr, pid, attr, size, flags);
}

int sched_yield()
{
	return syscall(__NR_sched_yield);
}

struct timespec timespec_diff(struct timespec start, struct timespec end)
{
	struct timespec temp;
	if ((end.tv_nsec - start.tv_nsec) < 0)
	{
		temp.tv_sec = end.tv_sec - start.tv_sec - 1;
		temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
	}
	else
	{
		temp.tv_sec = end.tv_sec - start.tv_sec;
		temp.tv_nsec = end.tv_nsec - start.tv_nsec;
	}
	return temp;
}

static void print_usage(const char *prog)
{
	printf("Usage: %s [-DsbdlHOLC3vpNR24SI]\n", prog);
	puts("  -a --acceleromoeter_device   device to use for the accelerometer (default /dev/spidev1.1)\n"
		 "   -g --gyro_device device to use for the gyro"
		 "  -v --verbose  Verbose (show tx buffer)\n"
		 "  -l --loopcount number of times to read acceleration\n"
		 "  -d --delay	delay in nanoseconds between reads of acceleration\n"
		 "  -s --sleep use nanosleep instead of the scheduler to delay\n"
		 "  -f --frequency motor drive frequency\n"
		 "  -o --outputfile file to output sensor data to\n");
	exit(1);
}

static void parse_opts(int argc, char *argv[])
{
	while (1)
	{
		static const struct option lopts[] = {
			{"acceleromoeter_device ", required_argument, 0, 'a'},
			{"gyro_device ", required_argument, 0, 'g'},
			{"verbose", no_argument, 0, 'v'},
			{"loopcount", required_argument, 0, 'l'},
			{"delay", required_argument, 0, 'd'},
			{"sleep", no_argument, 0, 's'},
			{"frequency", required_argument, 0, 'f'},
			{"outputfile", required_argument, 0, 'o'},
			{NULL, 0, 0, 0},
		};
		int c;

		c = getopt_long(argc, argv, "vsa:l:d:g:f:o:",
						lopts, NULL);

		if (c == -1)
			break;

		switch (c)
		{
		case 'a':
			accDevice = optarg;
			break;
		case 'g':
			gyroDevice = optarg;
			break;
		case 'v':
			verbose = 1;
			break;
		case 'l':
			loopCount = atoi(optarg);
			break;
		case 'd':
			lDelay = atoll(optarg);
			break;
		case 's':
			sleepInsteadOfSchedDeadline = true;
			break;
		case 'f':
			frequency = strtof(optarg, NULL);
			break;
		case 'o':
			outputFileName = optarg;
			break;
		default:
			print_usage(argv[0]);
			break;
		}
	}
}

void schedule_as_thread(uint64_t delay)
{
	int ret;
	struct sched_attr attr;

	printf("# sleeping by yeilding to the scheduler\n");
	ret = sched_getattr(0, &attr, sizeof(attr), 0);
	if (ret < 0)
		perror("01: ");
	attr.sched_policy = SCHED_DEADLINE;
	attr.sched_runtime = 1000000L;
	attr.sched_deadline = delay;
	ret = sched_setattr(0, &attr, 0);
	if (ret < 0)
	{
		perror("02: ");
	}
}

void accelerometer_thread(std::string devName, uint64_t delay, std::shared_ptr<Accelerometer> mySensorPtr)
{
	int yieldVal;
	schedule_as_thread(delay);
	while (true)
	{
		mySensorPtr->readAccelerometer();
		yieldVal = sched_yield();
		if (yieldVal != 0)
		{
			printf("problem with yield\n");
			perror(NULL);
		}
	}
}

void gyro_thread(std::string devName, uint64_t delay, std::shared_ptr<Gyro> mySensorPtr)
{
	int yieldVal;
	schedule_as_thread(delay);
	while (true)
	{
		mySensorPtr->readGyro();
		yieldVal = sched_yield();
		if (yieldVal != 0)
		{
			printf("problem with yield\n");
			perror(NULL);
		}
	}
}

void print_vals_thread(uint64_t delay, std::shared_ptr<Accelerometer> myAccPtr, std::shared_ptr<Gyro> myGyroPtr)
{
	int yieldVal;
	schedule_as_thread(delay);
	uint64_t loopCount = 0;
	while (true)
	{

		fprintf(fptr,
				"%05lld\t%05d\t%05d\t%05d\t%05d\t%05d\t%05d\n",
				loopCount,
				myAccPtr->get_latest_X(),
				myAccPtr->get_latest_Y(),
				myAccPtr->get_latest_Z(),
				myGyroPtr->get_latest_X(),
				myGyroPtr->get_latest_Y(),
				myGyroPtr->get_latest_Z()
				//accVals.size()
		);
		loopCount++;
		yieldVal = sched_yield();
		if (yieldVal != 0)
		{
			printf("problem with yield\n");
			perror(NULL);
		}
	}
}

void setup_sensor_file()
{
	fptr = fopen(outputFileName.c_str(), "w");
	if (fptr == NULL)
	{
		printf("#! No file specified using stdout\n");
		fptr = stdout;
	}
}

std::vector<float> transform_XYspin_to_m0m1m2(float X, float Y, float spin)
{
	std::vector<float> motorValues;
	printf("X: %f y: %f, spin: %f\n",X,Y,spin);
	motorValues.push_back((((-1.0 * (sin(M_PI / 3.0) * X)) + (cos(M_PI / 3.0) * Y )+ spin) / 3.0));
	motorValues.push_back(((-Y + spin) / 3.0));
	motorValues.push_back((((sin(M_PI / 3.0) * X) + (cos(M_PI / 3.0) * Y) + spin) / 3.0));
	printf("transformed X: %05.05f Y: %05.05f rot: %05.05f into m0: %05.05f m1: %05.05f m2: %05.05f\n",
		   X,
		   Y,
		   spin,
		   motorValues.at(0),
		   motorValues.at(1),
		   motorValues.at(2));
	return motorValues;
}

int main(int argc, char *argv[])
{
	printf("# compilied on %s:%s\n", __DATE__, __TIME__);
	std::unique_ptr<std::thread> accThreadPtr, gyroThreadPtr, printThreadPtr;

	parse_opts(argc, argv);

	auto mMotor = Motors(frequency, "/opt/robot/motors/");

	printf("# spi mode: 0x%x\n", mode);
	printf("# bits per word: %d\n", bits);
	printf("# max speed: %d Hz (%d KHz)\n", speed, speed / 1000);
	printf("# sleeping for %09lld nanoseconds between measurments\n", lDelay);

	setup_sensor_file();

	auto mAccPtr = std::make_shared<Accelerometer>(accDevice);
	mAccPtr->initalizeAccelerometer();
	try
	{

		accThreadPtr.reset(new std::thread(accelerometer_thread, accDevice, lDelay, mAccPtr));
		accThreadPtr->detach();
	}
	catch (const std::system_error &e)
	{
		std::cout << "Caught system_error with code " << e.code()
				  << " meaning " << e.what() << '\n';
	}
	auto mGyroPtr = std::make_shared<Gyro>(gyroDevice);
	mGyroPtr->initalizeGyro();
	try
	{
		gyroThreadPtr.reset(new std::thread(gyro_thread, gyroDevice, lDelay, mGyroPtr));
		gyroThreadPtr->detach();
	}
	catch (const std::system_error &e)
	{
		std::cout << "Caught system_error with code " << e.code()
				  << " meaning " << e.what() << '\n';
	}
	fprintf(fptr, "loopcount:\tACC_X:\tACC_Y:\tACC_Z:\tGYRO_X:\tGYRO_Y:\tGYRO_Z:\n");

	try
	{
		printThreadPtr.reset(new std::thread(print_vals_thread, lDelay * 40, mAccPtr, mGyroPtr));
		printThreadPtr->detach();
	}
	catch (const std::system_error &e)
	{
		std::cout << "Caught system_error with code " << e.code()
				  << " meaning " << e.what() << '\n';
	}

	static constexpr float ADJUST_AMOUNT = 1.0;
	char str;
	float X = 0.0;
	float Y = 0.0;
	float spin = 0.0;
	while (true)
	{
		str = getchar();
		switch (str)
		{
		case 'x':
			fflush(fptr);
			mMotor.set_m0m1m2(0.0,0.0,0.0);
			exit(1);
		case 'w':
			X += ADJUST_AMOUNT;
			break;
		case 's':
			X -= ADJUST_AMOUNT;
			break;
		case 'a':
			Y += ADJUST_AMOUNT;
			break;
		case 'd':
			Y -= ADJUST_AMOUNT;
			break;
		case 'q':
			spin += ADJUST_AMOUNT;
			break;
		case 'e':
			spin -= ADJUST_AMOUNT;
			break;
		}
		auto motorVals = transform_XYspin_to_m0m1m2(X, Y, spin);
		mMotor.set_m0m1m2(motorVals.at(0), motorVals.at(1), motorVals.at(2));
	}

	return 0;
}
