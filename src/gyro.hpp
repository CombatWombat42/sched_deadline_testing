#include "sensor.hpp"
#include "LSM9DS0.hpp"
class Gyro : public Sensor
{
public:
    Gyro(std::string filename) : Sensor(filename)
    {
        
    }
    void initalizeGyro()
    {
        if (mDebug)
        {
            printf("# initalizing gyro\n");
        }
        mStartAddr = LSM9DS0::GYRO_DATA_START_REG_ADDR;
        tx[0] = (LSM9DS0::GYRO_ENABLE_REG);
        tx[1] = ((LSM9DS0::GYRO_ENABLE) | ((LSM9DS0::GYRO_X_EN | LSM9DS0::GYRO_Y_EN | LSM9DS0::GYRO_Z_EN)));
        transfer(2);
    }
    void readGyro()
    {
        read_sensor_data();
    }
};