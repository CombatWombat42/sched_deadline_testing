#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string>
#include <vector>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include "LSM9DS0.hpp"

#ifndef SENSOR_H
#define SENSOR_H
class Sensor
{
private:
    std::vector<int16_t> latestVals;

protected:
    /**
     * @brief the start addr to read from
    * 
    */
    uint8_t mStartAddr;
    /**
     * @brief the file descriptor to use 
     * 
     */
    int mFd;
    /**
     * @brief number of bits per spi byte
     * 
     */
    uint8_t mBits;
    /**
     * @brief If nonzero, how long to delay after the last bit transfer
    *	before optionally deselecting the device before the next transfer
     * 
     */
    uint32_t mSpeed;
    /**
     * @brief spi mode (I don't know what this is)
     * 
     */
    uint32_t mMode;
    /**
      * @brief whether to display debugging messages
      * 
      */
    uint16_t mDelayUsec;
    /**
     * @brief the spi clock speed
     * 
     */
    bool mDebug;
    /**
     * @brief number of bytes in the spi address
     * 
     */
    static const int ADDR_BYTES = 1;
    /**
     * @brief the number of bytes the sensor produces
     * 
     */
    static const int SENSOR_BYTES = 2;
    /**
     * @brief number of axis on the sensor
     * 
     */
    static const int NUM_AXIS = 3;
    /**
     * @brief how many bytes to read to get data from all axis
     * 
     */
    static const int ALL_AXIS_READ_MSG_LEN = ADDR_BYTES + (SENSOR_BYTES * NUM_AXIS);
    /**
     * @brief the maximum number of bytes in our spi transfer
     * 
     */
    static const int MAX_SPI_XFER = 100;
    /**
     * @brief the buffer to send data from
     * 
     */
    uint8_t tx[MAX_SPI_XFER] = {
        0,
    };
    /**
     * @brief the buffer to receive data into
     * 
     */
    uint8_t rx[MAX_SPI_XFER] = {
        0,
    };

    /**
    * @brief print error message and abort
    * 
    * @param prefix a string to prefix the error with
    */
    static void pabort(std::string prefix)
    {
        perror(prefix.c_str());
        abort();
    }

    /**
    * @brief Set up the file descriptor used to access the spi device
    * 
    * @param device the filename to open
    */
    void setup_fd(std::string device)
    {
        int ret;
        printf("# setting up %s\n", device.c_str());
        mFd = open(device.c_str(), O_RDWR);
        if (mFd < 0)
            pabort("can't open device");

        /*
	    * spi mode
	    */
        ret = ioctl(mFd, SPI_IOC_WR_MODE32, &mMode);
        if (ret == -1)
            pabort("can't set spi mode");

        ret = ioctl(mFd, SPI_IOC_RD_MODE32, &mMode);
        if (ret == -1)
            pabort("can't get spi mode");

        /*
	    * bits per word
	    */
        ret = ioctl(mFd, SPI_IOC_WR_BITS_PER_WORD, &mBits);
        if (ret == -1)
            pabort("can't set bits per word");

        ret = ioctl(mFd, SPI_IOC_RD_BITS_PER_WORD, &mBits);
        if (ret == -1)
            pabort("can't get bits per word");

        /*
	    * max speed hz
	    */
        ret = ioctl(mFd, SPI_IOC_WR_MAX_SPEED_HZ, &mSpeed);
        if (ret == -1)
            pabort("can't set max speed hz");

        ret = ioctl(mFd, SPI_IOC_RD_MAX_SPEED_HZ, &mSpeed);
        if (ret == -1)
            pabort("can't get max speed hz");
    }

protected:
    /**
     * @brief debugging function to print the hex data. Really should live somewhere more genric
     * 
     * @param src the buffer of data to print from
     * @param length number of bytes to print
     * @param line_size number of bytes per line
     * @param prefix a string to prefix the line with 
     */
    static void hex_dump(const void *src, size_t length, size_t line_size,
                         std::string prefix)
    {
        int i = 0;
        const unsigned char *address = (const unsigned char *)src;
        const unsigned char *line = address;
        unsigned char c;

        printf("%s | ", prefix.c_str());
        while (length-- > 0)
        {
            printf("%02X ", *address++);
            if (!(++i % line_size) || (length == 0 && i % line_size))
            {
                if (length == 0)
                {
                    while (i++ % line_size)
                        printf("__ ");
                }
                printf(" |");
                while (line < address)
                {
                    c = *line++;
                    printf("%c", (c < 32 || c > 126) ? '.' : c);
                }
                printf("|\n");
                if (length > 0)
                    printf("%s | ", prefix.c_str());
            }
        }
    }

    /**
     * @brief read 2 bytes from each accesss 
     */
    void read_sensor_data()
    {
        tx[0] = ((LSM9DS0::SPI_READ) | (LSM9DS0::SPI_AUTO_ADDR_INC) | (mStartAddr));
        transfer(ALL_AXIS_READ_MSG_LEN);
        int16_t measuredX = (rx[2] << 8) | (rx[1]);
        int16_t measuredY = (rx[4] << 8) | (rx[3]);
        int16_t mesauredZ = (rx[6] << 8) | (rx[5]);
        //printf("%05d\t%05d\t%05d\n", measuredX, measuredY, mesauredZ);
        latestVals[0] = measuredX;
        latestVals[1] = measuredY;
        latestVals[2] = mesauredZ;
    }

    /**
     * @brief transfer data to the spi device an capture it's responses
     * 
     * @param tx buffer to transmit from
     * @param rx buffer to capture data into
     * @param len number of bytes to transfer
     */
    void transfer(size_t len)
    {
        int ret;
        //printf("# reading from fd %d\n",fd);
        struct spi_ioc_transfer tr = {
            .tx_buf = (unsigned long)tx,
            .rx_buf = (unsigned long)rx,
            .len = len,
            .speed_hz = mSpeed,
            .delay_usecs = mDelayUsec,
            .bits_per_word = mBits,
        };

        ret = ioctl(mFd, SPI_IOC_MESSAGE(1), &tr);
        if (ret < 1)
            pabort("can't send spi message");

        if (mDebug)
            hex_dump(tx, len, 32, "TX");

        if (mDebug)
            hex_dump(rx, len, 32, "RX");
    }

public:
    /**
     * @brief Construct a new Sensor object and open the spi device
     * 
     * @param filename the dev file 
     * @param speed speed to use for the spi bus
     * @param bits bits per transaction
     * @param mode spi mode (not sure what this does)
     * @param delayUsec If nonzero, how long to delay after the last bit transfer
     *	before optionally deselecting the device before the next transfer
     * @param debug should the object print debugging info
     */
    Sensor(std::string filename,
           uint32_t speed = 10000000L,
           uint8_t bits = 8,
           uint8_t mode = 0,
           uint16_t delayUsec = 0,
           bool debug = false)
        : mBits(bits), mSpeed(speed), mMode(mode), mDelayUsec(delayUsec), mDebug(debug)
    {
        latestVals.resize(3);
        setup_fd(filename);
        if (mDebug)
        {
            printf("# spi mode: 0x%x\n", mode);
            printf("# bits per word: %d\n", bits);
            printf("# max speed: %d Hz (%d KHz)\n", speed, speed / 1000);
        }
    }

    /**
 * @brief Get the latest values the sensor has read
 * 
 * @return std::vector<uint16_t> the values in x,y,x
 */
    std::vector<int16_t> get_latest_vals()
    {
        return latestVals;
    }

    int16_t get_latest_X()
    {
        return latestVals.at(0);
    }
    int16_t get_latest_Y()
    {
        return latestVals.at(1);
    }
    int16_t get_latest_Z()
    {
        return latestVals.at(2);
    }
};
#endif // SENSOR_H