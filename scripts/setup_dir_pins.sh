#!/usr/bin/env bash

#gpio 60
config-pin P9.12 gpio
#gpio 4
config-pin P9.18 gpio
#gpio 45
config-pin P8.11 gpio

echo out > /opt/robot/motors/dir0/direction
echo out > /opt/robot/motors/dir1/direction
echo out > /opt/robot/motors/dir2/direction
