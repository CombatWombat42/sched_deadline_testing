This is a tool to allow me to learn how to use SCHED_DEADLINE and spidev. The intent is to eventually get an old LSM9DS0 protoboard from sparkfun I have lying around and being read with little to no jitter. This *should* allow me to build an inverted pendilum robot, but we shall see.

The SCHED_DEADLINE stuff is stolen from [rt-app](https://github.com/scheduler-tools/rt-app).

The SPI stuff is stolen from the [kernel example](https://elixir.bootlin.com/linux/v5.4/source/tools/spi/spidev_test.c).

Gnuplot can be used on the output of the system to get acceleration values and timing values.

Right now it's hooked up to a Raspberry Pi4

# TODO
1. Figure out what the CPU is doing during the ioctl that get's the data from the SPI bus
1. Figure out how I'm going to do multiple threads/processes
1. Convert to c++
1. Figure out the nessicary bandwith of the accelerometer
    1. Figure out if I need to implement the fifo
    1. Figure out if I can read the accelerometer faster than the nessicary bandwith 
1. Figure out the gyro
1. Figure out PWM in c

# Done
1. Figure out how to make sub lists in markdown
1. Figure out if the schedule vs nanosleep parameter i just added actually works


# Errors

# Resolved

## struct redefention

[It looks like I'm using c/linux "wrong"](https://stackoverflow.com/questions/62677390/struct-included-in-two-header-files-neither-of-which-i-own/)

I get the following error when I un-comment ```#define _BITS_TYPES_STRUCT_SCHED_PARAM 1``` in LSM9DS0.cpp:

```
> Executing task in folder sched_deadline_testing: arm-linux-gnueabihf-g++ -o LSM9DS0 src/LSM9DS0.cpp -Wall --pedantic <

In file included from src/LSM9DS0.cpp:36:
/usr/arm-linux-gnueabihf/include/linux/sched/types.h:7:8: error: redefinition of ‘struct sched_param’
 struct sched_param {
        ^~~~~~~~~~~
In file included from /usr/arm-linux-gnueabihf/include/bits/sched.h:74,
                 from /usr/arm-linux-gnueabihf/include/sched.h:43,
                 from /usr/arm-linux-gnueabihf/include/pthread.h:23,
                 from /usr/arm-linux-gnueabihf/include/c++/8/arm-linux-gnueabihf/bits/gthr-default.h:35,
                 from /usr/arm-linux-gnueabihf/include/c++/8/arm-linux-gnueabihf/bits/gthr.h:148,
                 from /usr/arm-linux-gnueabihf/include/c++/8/ext/atomicity.h:35,
                 from /usr/arm-linux-gnueabihf/include/c++/8/bits/basic_string.h:39,
                 from /usr/arm-linux-gnueabihf/include/c++/8/string:52,
                 from src/LSM9DS0.cpp:20:
/usr/arm-linux-gnueabihf/include/bits/types/struct_sched_param.h:23:8: note: previous definition of ‘struct sched_param’
 struct sched_param
        ^~~~~~~~~~~
In file included from src/gyro.hpp:1,
                 from src/LSM9DS0.cpp:40:
src/sensor.hpp: In member function ‘void Sensor::transfer(size_t)’:
src/sensor.hpp:217:13: warning: C++ designated initializers only available with -std=c++2a or -std=gnu++2a [-Wpedantic]
             .tx_buf = (unsigned long)tx,
```

# Beaglebone black

## PWM

Note, you have to set a period before you can enable no one on the internet says that...

Note, you can not set a period less than the curren duty cycle, and you can't set a duty cycle higher than the current period, so set the duty cycle to 0 before changing the period.

[this is helpful](https://briancode.wordpress.com/2015/01/06/working-with-pwm-on-a-beaglebone-black/)

[this provided a memory to pin mapping](https://github.com/adafruit/adafruit-beaglebone-io-python/blob/1.2/source/common.c#L290)

| EXPORT NUMBER	| PIN NAME |	PINS |
|---------------|----------|---------|
|0      |EHRPWM0A|	P9.22,P9.31|
|1      |EHRPWM0B|	P9.21,P9.29|
|2      |ECAPPWM0|	P9.42|
|3      |EHRPWM1A|	P9.14,P8.36|
|4      |EHRPWM1B|	P9.16,P8.34|
|5      |EHRPWM2A|	P8.19,P8.45|
|6      |EHRPWM2B|	P8.13,P8.46|
|7      |ECAPPWM2|	P9.28|

## SPI

Note the device tree overlay is not modified in /boot/uEnv.txt

```
#uboot_overlay_addr4=/lib/firmware/BB-SPIDEV0-00A0.dtbo
#uboot_overlay_addr5=/lib/firmware/BB-SPIDEV1-00A0.dtbo
```

it looks like enabeling those lines will replace 
```
config-pin P9.28 spi_cs
config-pin P9.31 spi_sclk
config-pin P9.29 spi
config-pin P9.30 spi
```

**but** we still need to so ```config-pin P9.42 spi_cs``` apparently

[also of note, P9.42 is the other chip select](https://stackoverflow.com/questions/24078938/bbb-trouble-getting-second-spi-chip-select-with-device-tree)
